package com.sda.javagda17.students.controller;

import com.sda.javagda17.students.model.Student;
import com.sda.javagda17.students.model.dto.StudentDto;
import com.sda.javagda17.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/add")
    public String getStudentForm() {
        return "student/studentForm";
    }

    @PostMapping
    public String submitStudent(StudentDto student) {
        Optional<Student> studentOptional = studentService.addStudent(student);
        if (studentOptional.isPresent()) {
            return "redirect:/add";
        }

        return "redirect:/add";
    }
//    @PostMapping("/addStudent")
//    public String submitStudent(@RequestParam(name = "imie") String imie,
//                                @RequestParam(name = "nazwisko") String nazwisko,
//                                @RequestParam(name = "pesel") String pesel,
//                                @RequestParam(name = "index") String index) {
//        Student student = new Student();
//        student.setImie(imie);
//        student.setNazwisko(nazwisko);
//        student.setIndeks(index);
//        student.setPesel(pesel);
//
//        Optional<Student> studentOptional = studentService.addStudent(student);
//        if (studentOptional.isPresent()) {
//            return "redirect:/add";
//        }
//
//        return "redirect:/add";
//    }

    @GetMapping("/list")
    public String getStudentList(Model model) {
        model.addAttribute("studentList", studentService.getAll());
        return "student/list";
    }

    @GetMapping("/removeStudent")
    public String removeStudent(@RequestParam(name = "studentId") Long studentId) {
        studentService.remove(studentId);

        return "redirect:/list";
    }
}
