package com.sda.javagda17.students.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddStudentToJournalRequest {
    private Long studentId;
    private Long journalId;
}
