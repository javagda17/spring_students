package com.sda.javagda17.students.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String imie, nazwisko, indeks;

    @Column(unique = true)
    private String pesel;

}
