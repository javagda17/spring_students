package com.sda.javagda17.students.mapper;

import com.sda.javagda17.students.model.Student;
import com.sda.javagda17.students.model.dto.StudentDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface StudentMapper {

    @Mappings(value = {
            @Mapping(target = "imie", source = "studentImie"),
            @Mapping(target = "nazwisko", source = "studentNazwisko"),
            @Mapping(target = "indeks", source = "studentIndeks"),
            @Mapping(target = "pesel", source = "studentPesel"),
            @Mapping(target = "id", source = "studentId")
    })
    Student studentDtoToStudent(StudentDto dto);


    @Mappings(value = {
            @Mapping(source = "imie", target = "studentImie"),
            @Mapping(source = "nazwisko", target = "studentNazwisko"),
            @Mapping(source = "indeks", target = "studentIndeks"),
            @Mapping(source = "pesel", target = "studentPesel"),
            @Mapping(source = "id", target = "studentId")
    })
    StudentDto studentToStudentDto(Student student);
}
