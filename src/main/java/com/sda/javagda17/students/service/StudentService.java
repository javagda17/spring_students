package com.sda.javagda17.students.service;

import com.sda.javagda17.students.mapper.StudentMapper;
import com.sda.javagda17.students.model.Student;
import com.sda.javagda17.students.model.dto.StudentDto;
import com.sda.javagda17.students.repository.StudentRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

@Log
@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentMapper studentMapper;

    public Optional<Student> addStudent(StudentDto studentDto) {
        Student student = studentMapper.studentDtoToStudent(studentDto);

        try {
            return Optional.of(studentRepository.saveAndFlush(student));
        } catch (Exception cvee) {
            log.log(Level.SEVERE, "Duplikat studenta.");
        }
        return Optional.empty();
    }

    public List<StudentDto> getAll() {
        return studentRepository.findAll().stream()
                .map(student -> studentMapper.studentToStudentDto(student))
                .collect(Collectors.toList());
    }

    public void remove(Long studentId) {
        studentRepository.deleteById(studentId);
    }
}
